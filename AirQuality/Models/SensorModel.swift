//
//  SensorModel.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 08.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class SensorModel {
    
    var id: Int?
    var description: String?
    var short: String?
    
    init(id: Int? = nil, description: String? = nil, short: String? = nil) {
        self.id = id
        self.description = description
        self.short = short
    }
}
