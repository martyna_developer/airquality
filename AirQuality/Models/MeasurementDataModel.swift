//
//  MeasurementData.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 09.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class MeasurementDataModel {
    
    var value: Double?
    var date: Date?
    
    init(value: Double? = nil, date: Date? = nil) {
        self.value = value
        self.date = date
    }
}
