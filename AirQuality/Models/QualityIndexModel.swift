//
//  QualityIndexModel.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 12.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class QualityIndexModel {
    
    var id: Int?
    var stCalcDate: String?
    var indexLevelName: String?
    var so2CalcDate: String?
    var so2IndexLevel: String?
    var no2CalcDate: String?
    var no2IndexLevel: String?
    var coCalcDate: String?
    var coIndexLevel: String?
    var pm10CalcDate: String?
    var pm10IndexLevel: String?
    var pm25CalcDate: String?
    var pm25IndexLevel: String?
    var o3CalcDate: String?
    var o3IndexLevel: String?
    var c6h6CalcDate: String?
    var c6h6IndexLevel: String?
    
    init(id: Int? = nil, stCalcDate: String? = nil, indexLevelName: String? = nil, so2CalcDate: String? = nil, so2IndexLevel: String? = nil, no2CalcDate: String? = nil, no2IndexLevel: String? = nil, coCalcDate: String? = nil, coIndexLevel: String? = nil, pm10CalcDate: String? = nil, pm10IndexLevel: String? = nil, pm25CalcDate: String? = nil, pm25IndexLevel: String? = nil, o3CalcDate: String? = nil, o3IndexLevel: String? = nil, c6h6CalcDate: String? = nil, c6h6IndexLevel: String? = nil) {
        self.id = id
        self.stCalcDate = stCalcDate
        self.indexLevelName = indexLevelName
        self.so2CalcDate = so2CalcDate
        self.so2IndexLevel = so2IndexLevel
        self.no2CalcDate = no2CalcDate
        self.no2IndexLevel = no2IndexLevel
        self.coCalcDate = coCalcDate
        self.coIndexLevel = coIndexLevel
        self.pm10CalcDate = pm10CalcDate
        self.pm10IndexLevel = pm10IndexLevel
        self.pm25CalcDate = pm25CalcDate
        self.pm25IndexLevel = pm25IndexLevel
        self.o3CalcDate = o3CalcDate
        self.o3IndexLevel = o3IndexLevel
        self.c6h6CalcDate = c6h6CalcDate
        self.c6h6IndexLevel = c6h6IndexLevel
    }
}
