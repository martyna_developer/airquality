//
//  StationModel.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 06.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class StationModel {
   
    var id: Int?
    var title: String?
    var province: String?
    var street: String?

    init(id: Int? = nil, title: String? = nil, province: String? = nil, street: String? = nil) {
        self.id = id
        self.title = title
        self.province = province
        self.street = street
    }
}
