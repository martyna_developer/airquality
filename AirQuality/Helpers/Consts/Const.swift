//
//  Const.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 07.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class Const {
    
    //URLs to API
    static let stationsURL = "http://api.gios.gov.pl/pjp-api/rest/station/findAll"
    static let sensorsURL = "http://api.gios.gov.pl/pjp-api/rest/station/sensors/"
    static let measurementsURL = "http://api.gios.gov.pl/pjp-api/rest/data/getData/"
    static let ariQualityIndexURL = "http://api.gios.gov.pl/pjp-api/rest/aqindex/getIndex/"
}
