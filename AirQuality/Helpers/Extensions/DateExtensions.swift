//
//  DateExtensions.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 12.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation

func formatStringToDate(dateString: String) -> Date? {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let date = formatter.date(from: dateString)
    if let date = date {
        return date
    } else {
        return nil
    }
}

func formatDateToString(date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "HH:mm, dd-MM-yyyy"
    let dateString = formatter.string(from: date)
    return dateString
}

func formatDateToStringHour(date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "HH"
    let dateString = formatter.string(from: date)
    return dateString
}
