//
//  ColorExtensions.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 12.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {

    class var navigationBarColor: UIColor {
        return UIColor(red: 48.0/255.0, green: 160.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
}
