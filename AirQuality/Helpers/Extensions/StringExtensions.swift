//
//  StringExtensions.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 14.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation

extension String {
    
    func contains(_ find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
    func containsIgnoringCase(_ find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}
