//
//  ChartHelper.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 14.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import UIKit
import Foundation
import Charts

@objc(BarChartFormatter)
public class BarChartFormatter: NSObject, IAxisValueFormatter{
    
    var hours: [String] = [String]()
    
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        
        return hours[Int(value)]
    }
}
