//
//  MeasurementDataViewController.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 09.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit
import Charts

class MeasurementDataViewController: UIViewController, ChartViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var lastMeasurementHeaderLabel: UILabel!
    @IBOutlet weak var lastMeasurementValueLabel: UILabel!
    @IBOutlet weak var lastMeasurementDataLabel: UILabel!
    @IBOutlet weak var chartHeaderLabel: UILabel!
    @IBOutlet var chartView: BarChartView!
    
    var measurementsModel = MeasurementsModel()
    var hours = [String]()
    var sensorId: Int?
    var navigationBarTitle: String?
    var refreshControl: UIRefreshControl!
    var chartValues: [Double] = [Double]()

    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.alwaysBounceVertical = true
        chartView.delegate = self
        self.lastMeasurementHeaderLabel.text = "Ostatni pomiar"
        self.chartHeaderLabel.text = "Wcześniejsze pomiary"
        title = navigationBarTitle
        addRefreshControl()
        initializeMeasurementsModel()
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            scrollView.refreshControl = refreshControl
        } else {
            scrollView.addSubview(refreshControl)
        }
    }
    
    @objc func refresh(refreshControl: UIRefreshControl) {
        initializeMeasurementsModel()
    }
    
    func getMeasurementsModel(_ completion: @escaping (_ success: Bool) -> Void) {
        if let sensorId = self.sensorId {
            APIController.sharedInstance.measurements.getMeasurementsModel("\(Const.measurementsURL)\(sensorId)", completion: { (success, measurement, error_msg) in
                if success {
                    self.measurementsModel.measurements.append(contentsOf: measurement)
                    self.refreshControl.endRefreshing()
                    return completion(true)
                } else {
                    self.refreshControl.endRefreshing()
                    return completion(false)
                }
            })
        }
    }
    func initializeMeasurementsModel() {
        measurementsModel.measurements.removeAll()
        getMeasurementsModel() { (success) in
            let values = self.measurementsModel.measurements
            if !values.isEmpty {
                self.prepareData()
                self.initializeChartView(values: self.chartValues)
                for value in values {
                    if let v = value.value {
                        self.lastMeasurementValueLabel.text = "\("Wartość: ")\(String(v))"
                        if let date = value.date {
                            self.lastMeasurementDataLabel.text = "\("Data: ")\(formatDateToString(date: date))"
                            return
                        }
                        return
                    }
                }
            } else {
                self.lastMeasurementValueLabel.text = "\("Wartość: nieznana")"
                self.lastMeasurementDataLabel.text = "\("Data: nieznana")"
            }
        }
    }
    
    func initializeChartView(values: [Double]) {
        var dataEntries: [BarChartDataEntry] = [BarChartDataEntry]()
        let formatter: BarChartFormatter = BarChartFormatter()
        let xaxis: XAxis = XAxis()
        formatter.hours = hours
        
        for i in 0..<values.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: Double(values[i]))
            dataEntries.append(dataEntry)
        }
        xaxis.valueFormatter = formatter
        chartView.xAxis.valueFormatter = xaxis.valueFormatter

        if let measurementTitle = self.title {
            let barChartDataSet = BarChartDataSet(values: dataEntries, label: "Wartość parametru \(measurementTitle)")
            barChartDataSet.drawValuesEnabled = false
            barChartDataSet.colors = [UIColor.blue]
            let chartData = BarChartData(dataSet: barChartDataSet)
            chartView.chartDescription?.text = ""
            chartView.xAxis.labelPosition = .bottom
            chartView.data = chartData
        }
    }
    
    func prepareData() {
        self.chartValues.removeAll()
        self.hours.removeAll()
        let reversedMeasurements = self.measurementsModel.measurements.reversed()
        let values = Array(reversedMeasurements)
        for i in 0..<values.count {
            if let value = values[i].value, let date = values[i].date {
                let d = formatDateToStringHour(date: date)
                if i == 0 {
                    self.hours.append("Godzina")
                } else {
                    self.hours.append(" \(d)")
                }
                self.chartValues.append(value)
            }
        }
    }
}

