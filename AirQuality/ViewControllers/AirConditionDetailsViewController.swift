//
//  AirConditionDetailsViewController.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 14.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class AirConditionDetailsViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var so2IndexLevelLabel: UILabel!
    @IBOutlet weak var no2IndexLevelLabel: UILabel!
    @IBOutlet weak var coIndexLevelLabel: UILabel!
    @IBOutlet weak var pm10IndexLevelLabel: UILabel!
    @IBOutlet weak var pm25IndexLevelLabel: UILabel!
    @IBOutlet weak var o3IndexLevelLabel: UILabel!
    @IBOutlet weak var c6h6IndexLevelLabel: UILabel!
    
    var indexList: QualityIndexModel?
 
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Szczegóły"
        scrollView.alwaysBounceVertical = true
        getDetailIndexes()
    }
    
    func getDetailIndexes() {
        if let so2 = indexList?.so2IndexLevel {
            self.so2IndexLevelLabel.text = "\("Stan SO2: ")\(so2)"
        } else {
                self.so2IndexLevelLabel.isHidden = true
        }
        if let no2 = indexList?.no2IndexLevel {
            self.no2IndexLevelLabel.text = "\("Stan NO2: ")\(no2)"
        } else {
            self.no2IndexLevelLabel.isHidden = true
        }
        if let co = indexList?.coIndexLevel {
            self.coIndexLevelLabel.text = "\("Stan CO: ")\(co)"
        } else {
            self.coIndexLevelLabel.isHidden = true
        }
        if let pm10 = indexList?.pm10IndexLevel {
            self.pm10IndexLevelLabel.text = "\("Stan pm10: ")\(pm10)"
        } else {
            self.pm10IndexLevelLabel.isHidden = true
        }
        if let pm25 = indexList?.pm25IndexLevel {
            self.pm25IndexLevelLabel.text = "\("Stan pm2.5: ")\(pm25)"
        } else {
            self.pm25IndexLevelLabel.isHidden = true
        }
        if let o3 = indexList?.o3IndexLevel {
            self.o3IndexLevelLabel.text = "\("Stan O3: ")\(o3)"
        } else {
            self.o3IndexLevelLabel.isHidden = true
        }
        if let c6h6 = indexList?.c6h6IndexLevel {
            self.c6h6IndexLevelLabel.text = "\("Stan C6H6: ")\(c6h6)"
        } else {
            self.c6h6IndexLevelLabel.isHidden = true
        }
    }
    
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

