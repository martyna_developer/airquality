//
//  SensorsListViewController.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 08.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class SensorsListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var sensorsTableView: UITableView!
    @IBOutlet weak var generalIndexLabel: UILabel!
    
    var sensorsList = SensorsListModel()
    var indexList = QualityIndexModel()
    var stationId: Int?
    var chosenSensorId: Int?
    var chosenSensorName: String?
    var refreshControl: UIRefreshControl!
    var chosenStationName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sensorsTableView.tableFooterView = UIView()
        sensorsTableView.dataSource = self
        sensorsTableView.delegate = self
        sensorsTableView.separatorStyle = .none
        addRefreshControl()
        initializeSensorList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let name = chosenStationName {
            title = name
        }
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            sensorsTableView.refreshControl = refreshControl
        } else {
            sensorsTableView.addSubview(refreshControl)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sensorsList.sensors.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 104.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.sensorsTableView.deselectRow(at: indexPath, animated: true)
        self.chosenSensorId = sensorsList.sensors[indexPath.row].id
        self.chosenSensorName = sensorsList.sensors[indexPath.row].short
        performSegue(withIdentifier: "showMeasurements", sender: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "airSensorCell") as! AirSensorCell
        cell.selectionStyle = .none
        cell.shadowView.addShadowToView()
        
        cell.paramFormula.text = sensorsList.sensors[indexPath.row].short
        cell.paramName.text = sensorsList.sensors[indexPath.row].description
        
        return cell
    }
    
    func getSensorsList(_ completion: @escaping (_ success: Bool) -> Void) {
        if let stationId = self.stationId {
        APIController.sharedInstance.sensors.getSensorsList("\(Const.sensorsURL)\(stationId)", completion: { (success, sensor, error_msg) in
            if success {
                self.sensorsList.sensors.append(contentsOf: sensor)
                self.sensorsTableView.reloadData()
                return completion(true)
            } else {
                return completion(false)
            }
        })
        }
    }
    
    func getAirIndex(_ completion: @escaping (_ success: Bool) -> Void) {
        if let stationId = self.stationId {
            APIController.sharedInstance.indexes.getQualityIndexList("\(Const.ariQualityIndexURL)\(stationId)", completion: { (success, sensor, error_msg) in
                if success {
                    if let s = sensor {
                        self.indexList = s
                    }
                    self.sensorsTableView.reloadData()
                    return completion(true)
                } else {
                    return completion(false)
                }
            })
        }
    }
    
    func initializeSensorList() {
        sensorsList.sensors.removeAll()
        getSensorsList() { (success) in
            self.refreshControl.endRefreshing()
        }
        getAirIndex() { (success) in
            if let index = self.indexList.indexLevelName {
                self.generalIndexLabel.text = "\("Ogólny stan powietrza: ")\(index)"
            } else {
                self.generalIndexLabel.text = "\("Ogólny stan powietrza: nieznany...")"
            }
            self.refreshControl.endRefreshing()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        title = ""
        if segue.identifier == "showMeasurements" {
            let destVC = segue.destination as? MeasurementDataViewController
            destVC?.sensorId = self.chosenSensorId
            destVC?.navigationBarTitle = self.chosenSensorName
        } else if segue.identifier == "showAirDetails" {
            let nav = segue.destination as? UINavigationController
            let destVC = nav?.topViewController as? AirConditionDetailsViewController
            destVC?.indexList = self.indexList
        }
    }
    
    @objc func refresh(refreshControl: UIRefreshControl) {
        initializeSensorList()
    }
    
    @IBAction func airDetailsButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "showAirDetails", sender: self)
    }
}
