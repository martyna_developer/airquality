//
//  StationsListViewController.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 06.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class StationsListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var stationsTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var stationsList = StationsListModel()
    var searchedStationsList = StationsListModel()
    var chosenStationID: Int?
    var chosenStationTitle: String?
    var refreshControl: UIRefreshControl!
    var isSearchModeOn = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        stationsTableView.tableFooterView = UIView()
        stationsTableView.dataSource = self
        stationsTableView.delegate = self
        stationsTableView.separatorStyle = .none
        addRefreshControl()
        initializeStationList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Stacje pomiarowe"
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            stationsTableView.refreshControl = refreshControl
        } else {
            stationsTableView.addSubview(refreshControl)
        }
    }
    
    @objc func refresh(refreshControl: UIRefreshControl) {
        initializeStationList()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !isSearchModeOn {
            return stationsList.stations.count
        } else {
            return searchedStationsList.stations.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 104.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "stationCell") as! AirStationCell
        cell.selectionStyle = .none
        cell.shadowView.addShadowToView()
        if !self.isSearchModeOn {
            cell.titleLabel.text = stationsList.stations[indexPath.row].title
            cell.provinceLabel.text = stationsList.stations[indexPath.row].province
            cell.streetLabel.text = stationsList.stations[indexPath.row].street
        } else {
            cell.titleLabel.text = searchedStationsList.stations[indexPath.row].title
            cell.provinceLabel.text = searchedStationsList.stations[indexPath.row].province
            cell.streetLabel.text = searchedStationsList.stations[indexPath.row].street
        }
        return cell
    }
    
    func getStationsList(_ completion: @escaping (_ success: Bool) -> Void) {
       
        APIController.sharedInstance.stations.getStationsList(Const.stationsURL, completion: { (success, station, error_msg) in
            if success {
                self.stationsList.stations.append(contentsOf: station)
                self.stationsTableView.reloadData()
                return completion(true)
            } else {
                return completion(false)
            }
        })
    }

    func initializeStationList() {
        stationsList.stations.removeAll()
        stationsTableView.reloadData()
        getStationsList() { (success) in
            self.refreshControl.endRefreshing()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.stationsTableView.deselectRow(at: indexPath, animated: true)
        if !self.isSearchModeOn {
            self.chosenStationID = stationsList.stations[indexPath.row].id
            self.chosenStationTitle = stationsList.stations[indexPath.row].title
        } else {
            self.chosenStationID = searchedStationsList.stations[indexPath.row].id
            self.chosenStationTitle = searchedStationsList.stations[indexPath.row].title
        }
        performSegue(withIdentifier: "showStationDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        title = ""
        if segue.identifier == "showStationDetails" {
            let destVC = segue.destination as? SensorsListViewController
            destVC?.stationId = self.chosenStationID
            destVC?.chosenStationName = self.chosenStationTitle
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != "" {
            self.isSearchModeOn = true
            self.searchedStationsList.stations.removeAll()
            for item in self.stationsList.stations {
                if let title = item.title, let province = item.province, let street = item.street {
                    if title.containsIgnoringCase(searchText) || province.containsIgnoringCase(searchText) || street.containsIgnoringCase(searchText) {
                        self.searchedStationsList.stations.append(item)
                    }
                }
            }
            self.stationsTableView.reloadData()
        } else {
            self.isSearchModeOn = false
            self.stationsTableView.reloadData()
        }
    }
}

