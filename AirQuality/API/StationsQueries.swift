//
//  StationsQueries.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 07.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class StationsQueries: APIBase {
    
    func getStationsList(_ url: String, completion: @escaping (_ success:Bool, _ stations: [StationModel], _ error_msg: String?) -> Void) {
        Alamofire.request(url, method: .get).responseJSON { response in
            if self.isInternetAvailable() {
                if response.result.isSuccess {
                    print(response.result.value)
                    if let a = response.result.value as? [[String: AnyObject]] {
                        if !self.isStatusValid(response.response?.statusCode, url: url) {
                            return completion(false, [], "Server communication error")
                        }
                        let stations = StationMapper.mapStation(response: a)
                        return completion(true, stations, nil)
                    }
                }
            }
            return completion(false, [], "Server communication error")
        }
    }
}
    
