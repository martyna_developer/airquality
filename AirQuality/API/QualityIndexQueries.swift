//
//  QualityIndexQueries.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 12.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class QualityIndexQueries: APIBase {
    
    func getQualityIndexList(_ url: String, completion: @escaping (_ success:Bool, _ qualityIndexes: QualityIndexModel?, _ error_msg: String?) -> Void) {
        Alamofire.request(url, method: .get).responseJSON { response in
            if self.isInternetAvailable() {
                if response.result.isSuccess {
                    print(response.result.value)
                    if let a = response.result.value as? [String: AnyObject] {
                        if !self.isStatusValid(response.response?.statusCode, url: url) {
                            return completion(false, nil, "Server communication error")
                        }
                        let qualityIndexModel = QualityIndexMapper.mapQualityIndex(response: a)
                        return completion(true, qualityIndexModel, nil)
                    }
                }
            }
            return completion(false, nil, "Server communication error")
        }
    }
}
