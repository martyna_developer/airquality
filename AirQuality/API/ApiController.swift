//
//  ApiController.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 07.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation

class APIController {
    
    static let sharedInstance = APIController()
    var stations : StationsQueries
    var sensors : SensorsQueries
    var measurements: MeasurementQueries
    var indexes: QualityIndexQueries

    fileprivate init() {
        stations = StationsQueries()
        sensors = SensorsQueries()
        measurements = MeasurementQueries()
        indexes = QualityIndexQueries()
    }
}
