//
//  AirSensorCell.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 08.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class AirSensorCell: UITableViewCell {

    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var paramName: UILabel!
    @IBOutlet weak var paramFormula: UILabel!
    
}
