//
//  AirCell.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 06.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class AirStationCell: UITableViewCell {
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var provinceLabel: UILabel!
    @IBOutlet weak var streetLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
