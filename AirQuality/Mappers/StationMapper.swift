//
//  StationMapper.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 07.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation

class StationMapper {
    
    class func mapStation(response: [[String: AnyObject]]) -> [StationModel] {
        var stations = [StationModel]()
        for station in response {
            let stationModel = StationModel()
            guard let id = station["id"] as? NSNumber else {
                continue
            }
            stationModel.id = id.intValue
            if let stationName = station["stationName"] as? String {
                stationModel.title = stationName
            }
            if let commune = station["city"]?["commune"] as? [String: AnyObject] {
                if let provinceName = commune["provinceName"] as? String {
                    stationModel.province = provinceName
                }
            }
            if let addressStreet = station["addressStreet"] as? String {
                stationModel.street = addressStreet
            }
            stations.append(stationModel)
        }
        let stationsOrdered = stations.filter{ $0.title != nil}.sorted(by: {
            if let name0 = $0.title, let name1 = $1.title {
                return name0.compare((name1)) == .orderedAscending
            }
            return false
        })
        return stationsOrdered
    }
}
