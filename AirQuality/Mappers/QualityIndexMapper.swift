//
//  QualityIndexMapper.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 12.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation

class QualityIndexMapper {
    
    class func mapQualityIndex(response: [String: AnyObject]) -> QualityIndexModel {
        let qualityIndexModel = QualityIndexModel()
        guard let id = response["id"] as? NSNumber else {
            return qualityIndexModel
        }
        qualityIndexModel.id = id.intValue
        if let generalAirQualityIndex = response["stIndexLevel"]?["indexLevelName"] as? String {
            qualityIndexModel.indexLevelName = generalAirQualityIndex
        }
        if let indexDate = response["stCalcDate"] as? String { qualityIndexModel.stCalcDate = indexDate
        }
        if let so2Date = response["so2CalcDate"] as? String {
            qualityIndexModel.so2CalcDate = so2Date
        }
        if let so2Index = response["so2IndexLevel"]?["indexLevelName"] as? String {
            qualityIndexModel.so2IndexLevel = so2Index
        }
        if let no2Date = response["no2CalcDate"] as? String {
            qualityIndexModel.no2CalcDate = no2Date
        }
        if let no2Index = response["no2IndexLevel"]?["indexLevelName"] as? String {
            qualityIndexModel.no2IndexLevel = no2Index
        }
        if let coDate = response["coCalcDate"] as? String {
            qualityIndexModel.coCalcDate = coDate
        }
        if let coIndex = response["coIndexLevel"]?["indexLevelName"] as? String {
            qualityIndexModel.coIndexLevel = coIndex
        }
        if let pm10Date = response["pm10CalcDate"] as? String {
            qualityIndexModel.pm10CalcDate = pm10Date
        }
        if let pm10Index = response["pm10IndexLevel"]?["indexLevelName"] as? String {
            qualityIndexModel.pm10IndexLevel = pm10Index
        }
        if let pm25Date = response["pm25CalcDate"] as? String {
            qualityIndexModel.pm25CalcDate = pm25Date
        }
        if let pm25Index = response["pm25IndexLevel"]?["indexLevelName"] as? String {
            qualityIndexModel.pm25IndexLevel = pm25Index
        }
        if let o3Date = response["o3CalcDate"] as? String {
            qualityIndexModel.o3CalcDate = o3Date
        }
        if let o3Index = response["o3IndexLevel"]?["indexLevelName"] as? String {
            qualityIndexModel.o3IndexLevel = o3Index
        }
        if let c6h6Date = response["c6h6CalcDate"] as? String {
            qualityIndexModel.c6h6CalcDate = c6h6Date
        }
        if let c6h6Index = response["c6h6IndexLevel"]?["indexLevelName"] as? String {
            qualityIndexModel.c6h6IndexLevel = c6h6Index
        }
        return qualityIndexModel
    }
}
