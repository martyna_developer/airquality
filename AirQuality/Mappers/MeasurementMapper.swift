//
//  MeasurementMapper.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 09.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation

class MeasurementMapper {
    
    class func mapMeasurements(response: [String: AnyObject]) -> [MeasurementDataModel] {
        var measurements = [MeasurementDataModel]()
        if let values = response["values"] as? [AnyObject] {
            for value in values {
                let measurement = MeasurementDataModel()
                if let dict = value as? [String: AnyObject] {
                    if let date = dict["date"] as? String {
                        measurement.date = formatStringToDate(dateString: date)
                        measurement.value = dict["value"] as? Double
                    }
                }
                measurements.append(measurement)
            }
        }
        return measurements
    }
}
