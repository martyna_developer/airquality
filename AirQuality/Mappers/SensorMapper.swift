//
//  SensorMapper.swift
//  AirQuality
//
//  Created by Martyna Wiśnik on 08.01.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import Alamofire

class SensorMapper {
    
    class func mapSensor(response: [[String: AnyObject]]) -> [SensorModel] {
        var sensors = [SensorModel]()
        for sensor in response {
            let sensorModel = SensorModel()
            guard let id = sensor["id"] as? NSNumber else {
                continue
            }
            sensorModel.id = id.intValue
            if let paramFormula = sensor["param"]?["paramFormula"] as? String {
                sensorModel.short = paramFormula
            }
            if let paramName = sensor["param"]?["paramName"] as? String {
                sensorModel.description = paramName
            }
            sensors.append(sensorModel)
        }
        return sensors
    }
}
